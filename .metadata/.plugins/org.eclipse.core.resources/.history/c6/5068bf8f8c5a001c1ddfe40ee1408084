package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "emp_table")
public class Employee {

	@Id
	@Column(name = "emp_id")
	private Long eid;

	@Column(name = "emp_name")
	@NotNull(message = "Name Should Not Be Null.")
	@NotEmpty(message = "Name Should Not Be Empty.")
	private String name;

	@Column(name = "emp_age")
	@NotNull(message = "Age Should Not Be Null.")
	@NotEmpty(message = "Age Should Not Be Empty.")
	private int age;

	@Column(name = "emp_contact")
	@NotNull(message = "Contact Should Not Be Null.")
	@NotEmpty(message = "Contact Should Not Be Empty.")
	private String contact;

	@Column(name = "emp_email")
	@NotNull(message = "Email Should Not Be Null.")
	@NotEmpty(message = "Email Should Not Be Empty.")
	private String email;

}
