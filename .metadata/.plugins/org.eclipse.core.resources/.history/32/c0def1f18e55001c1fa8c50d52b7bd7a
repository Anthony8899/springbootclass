package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employee;
import com.example.demo.service.EmployeeService;

@RestController
@RequestMapping("/api/emp")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@PostMapping("/add")
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee emp) {
		Employee empl = employeeService.addEmployee(emp);
		return new ResponseEntity<>(empl, HttpStatus.CREATED);
	}

	@GetMapping("/getAll")
	public ResponseEntity<List<Employee>> getAllEmployee() {
		List<Employee> fetchAllEmp = employeeService.fetchAllEmp();
		return new ResponseEntity<>(fetchAllEmp, HttpStatus.FOUND);
	}

	@GetMapping("/get/{eid}")
	public ResponseEntity<Employee> getAllEmployee(@PathVariable Long eid) {
		Employee emp = employeeService.getById(eid);
		return new ResponseEntity<>(emp, HttpStatus.FOUND);
	}
	
	@DeleteMapping("/del/{eid}")
	public ResponseEntity<String> deleteEmployee(@PathVariable Long eid) {
		String str = "";
		boolean status = employeeService.deleteById(eid);
		if(status) {
			str = "Record Deleted";
		}else {
			str = "Record Not Deleted";
		}
		return new ResponseEntity<>(str, HttpStatus.OK);
	}
	
	@PutMapping("/upd")
	public ResponseEntity<Employee> updEmployee(@RequestBody Employee emp) {
		Employee empl = employeeService.updEmployee(emp);
		return new ResponseEntity<>(empl, HttpStatus.OK);
	}

}
