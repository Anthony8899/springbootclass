package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.example.demo.controller.Airtel;
import com.example.demo.controller.Jio;
import com.example.demo.controller.Sim;

@Configuration
public class Config {

//	@Bean
//	public Sim airtel() {
//		return new Airtel();
//	}
//	
//	@Bean
//	public Sim jio() {
//		return new Jio();
//	}

	@Bean
	public Airtel airtel() {
		return new Airtel();
	}

	@Bean
	public Jio jio() {
		return new Jio();
	}
}
