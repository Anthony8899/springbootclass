package com.example.demo.controller;

public class Airtel{

	public String callService() {
		return "Airtel call Service Invoked";
	}

	public String dataService() {
		return "Airtel data Service Invoked";
	}

	public String msgService() {
		return "Airtel message Service Invoked";
	}
}
