package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sim")
public class Controllers {

	@Autowired
	Airtel airtel;

	@Autowired
	Jio jio;

	@GetMapping("/getAirtel")
	public String getAirtel() {
		String call = airtel.callService();
		String data = airtel.dataService();
		String msg = airtel.msgService();

		return call + " | " + data + " | " + msg;
	}

	@GetMapping("/getJio")
	public String getJio() {
		String call = jio.callService();
		String data = jio.dataService();
		String msg = jio.msgService();

		return call + " | " + data + " | " + msg;
	}

}
