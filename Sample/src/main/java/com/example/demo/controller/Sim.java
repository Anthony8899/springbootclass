package com.example.demo.controller;

public interface Sim {

	public String callService();
	
	public String dataService();
	
	public String msgService();
}
