package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeListPageDto {

	private Long eid;
	private String name;
	private String contact;
}
