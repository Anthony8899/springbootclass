package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.EmployeeListPageDto;
import com.example.demo.model.Employee;

public interface EmployeeService {

	public Employee addEmployee(Employee emp);
	
	public List<Employee> fetchAllEmp();
	
	public List<EmployeeListPageDto> fetchSomeEmp();
	
	public Employee getById(Long eid);
	
	public boolean deleteById(Long eid);
	
	public Employee updEmployee(Employee emp);
	
	public boolean deleteAllEmp();
}