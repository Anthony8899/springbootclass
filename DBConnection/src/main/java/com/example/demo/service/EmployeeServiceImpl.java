package com.example.demo.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.EmployeeListPageDto;
import com.example.demo.model.Employee;
import com.example.demo.repo.EmployeeRepo;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepo employeeRepo;

	@Override
	public Employee addEmployee(Employee emp) {
		Employee empl = employeeRepo.save(emp);
		return empl;
	}

	@Override
	public List<Employee> fetchAllEmp() {
		List<Employee> empLi = employeeRepo.findAll();
		return empLi;
	}

	@Override
	public Employee getById(Long eid) {
		Optional<Employee> emp = employeeRepo.findById(eid);
		if (emp.isPresent()) {
			return emp.get();
		}
		return null;
	}

	@Override
	public boolean deleteById(Long eid) {
		employeeRepo.deleteById(eid);
		return true;
	}

	@Override
	public Employee updEmployee(Employee emp) {
		Optional<Employee> e = employeeRepo.findById(emp.getEid());
		if (e.isPresent()) {
			Employee empl = employeeRepo.save(emp);
			return empl;
		}
		return null;
	}

	@Override
	public boolean deleteAllEmp() {
		employeeRepo.deleteAll();
		return true;
	}

	@Override
	public List<EmployeeListPageDto> fetchSomeEmp() {
		List<Employee> empLi = employeeRepo.findAll();
		ModelMapper mapper = new ModelMapper();
		List<EmployeeListPageDto> li = empLi.stream()
				.map(emp -> mapper.map(emp, EmployeeListPageDto.class))
				.collect(Collectors.toList());
		return li;
	}

}
