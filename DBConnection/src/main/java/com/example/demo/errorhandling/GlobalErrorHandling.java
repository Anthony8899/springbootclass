package com.example.demo.errorhandling;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.demo.customexception.DataNotFound;
import com.example.demo.exceptionresponse.ExceptionResponse;

@RestControllerAdvice
public class GlobalErrorHandling extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Map<String, String> err = new HashMap<>();
		err.put("timeStamp", new Date().toString());
		err.put("discription", "Validation Error");
		List<String> errors = e.getAllErrors().stream().map(error -> error.getDefaultMessage())
				.collect(Collectors.toList());
		err.put("message", errors.toString());
		ExceptionResponse res = new ExceptionResponse(new Date(), e.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(err, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ExceptionResponse> exception(Exception e) {
		ExceptionResponse res = new ExceptionResponse(new Date(), e.getMessage(), "Error Occurred");
		return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(DataNotFound.class)
	public ResponseEntity<ExceptionResponse> dataNotFoundException(DataNotFound e, WebRequest req) {
		ExceptionResponse res = new ExceptionResponse(new Date(), e.getMessage(), req.getDescription(false));
		return new ResponseEntity<>(res, HttpStatus.NOT_FOUND);
	}
}
