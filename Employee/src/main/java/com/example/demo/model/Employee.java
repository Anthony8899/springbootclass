package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Employee {

	private String eid;
	private String name;
	private int age;
	private String contact;
	private Long salary;
}
