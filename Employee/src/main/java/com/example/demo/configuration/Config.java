package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.service.EmployeeService;
import com.example.demo.service.EmployeeServiceImpl;

@Configuration
public class Config {

	@Bean
	public EmployeeService employeeService() {
		return new EmployeeServiceImpl();
	}
}
