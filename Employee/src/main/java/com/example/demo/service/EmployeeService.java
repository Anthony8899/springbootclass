package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Employee;

public interface EmployeeService {
	
	public boolean addEmployee(Employee emp);
	
	public List<Employee> getAllEmp();
	
	public Employee getEmployeeById(String eid);
	
	public void deleteEmployeeById(String eid);

}
