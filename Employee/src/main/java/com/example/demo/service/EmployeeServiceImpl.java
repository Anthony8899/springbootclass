package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	List<Employee> allEmployee = new ArrayList<>();

	@Override
	public boolean addEmployee(Employee emp) {
		boolean isInserted = allEmployee.add(emp);
		return isInserted;
	}

	@Override
	public List<Employee> getAllEmp() {
		return allEmployee;
	}

	@Override
	public Employee getEmployeeById(String eid) {
		Employee emp = allEmployee.stream().filter(p -> p.getEid().equals(eid)).findFirst().get();
		return emp;
	}

	@Override
	public void deleteEmployeeById(String eid) {
		Employee emp = allEmployee.stream().filter(p -> p.getEid().equals(eid)).findFirst().get();
		allEmployee.remove(emp);
	}

}
