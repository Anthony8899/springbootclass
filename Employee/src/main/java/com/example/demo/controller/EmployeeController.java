package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employee;
import com.example.demo.service.EmployeeService;

@RestController
@RequestMapping("/api/emp")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@PostMapping("/add")
	public boolean addEmp(@RequestBody Employee emp) {
		boolean status = employeeService.addEmployee(emp);
		return status;
	}

	@GetMapping("/getAll")
	public List<Employee> getAllEmp() {
		List<Employee> emp = employeeService.getAllEmp();
		return emp;
	}

//	@GetMapping("/get/{eid}")
//	public Employee getEmployeeById(@PathVariable String eid) {
//		Employee emp = employeeService.getEmployeeById(eid);
//		return emp;
//	}

	@GetMapping("/get")
	public Employee getEmployeeById(@RequestParam("eid") String eid) {
		Employee emp = employeeService.getEmployeeById(eid);
		return emp;
	}

	@DeleteMapping("/del/{eid}")
	public boolean delEmployeeById(@PathVariable String eid) {
		employeeService.deleteEmployeeById(eid);
		return true;
	}
}
